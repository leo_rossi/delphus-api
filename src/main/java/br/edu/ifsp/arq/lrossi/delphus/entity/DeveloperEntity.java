package br.edu.ifsp.arq.lrossi.delphus.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.time.LocalDateTime;
import java.util.Set;

@Data
@Table(name = DeveloperEntity.TABLE_NAME)
@Entity(name = DeveloperEntity.ENTITY_NAME)
public class DeveloperEntity {

    public static final String ENTITY_NAME = "DeveloperEntity";
    public static final String TABLE_NAME = "developer";
    private static final String ID_DEVELOPER = "id_developer";
    private static final String GIT_USERNAME = "git_username";
    private static final String CREATION_DATE = "creation_date";
    private static final String UPDATE_DATE = "update_date";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = ID_DEVELOPER, nullable = false)
    private Long id;

    @Column(name = GIT_USERNAME, nullable = false)
    private String username;

    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @OneToMany(mappedBy = "owner")
    private Set<ProjectEntity> projects;

    @CreationTimestamp
    @Column(name = CREATION_DATE, nullable = false)
    private LocalDateTime creationDate;

    @UpdateTimestamp
    @Column(name = UPDATE_DATE)
    private LocalDateTime updateDate;

    private Boolean active;
}
