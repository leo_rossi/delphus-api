package br.edu.ifsp.arq.lrossi.delphus.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Data
@Embeddable
public class ProjectLanguageEmbeddedId {

    private static final String ID_PROJECT = "id_project";
    private static final String ID_LANGUAGE = "id_language";

    @Column(name = ID_PROJECT)
    private Long projectId;

    @Column(name = ID_LANGUAGE)
    private Long languageId;

}
