package br.edu.ifsp.arq.lrossi.delphus.entity;

import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.time.LocalDateTime;

@Data
@Entity(name = ProjectLanguageEntity.ENTITY_NAME)
@Table(name = ProjectLanguageEntity.TABLE_NAME)
public class ProjectLanguageEntity {

    public static final String ENTITY_NAME = "ProjectLanguageEntity";
    public static final String TABLE_NAME = "project_language";
    private static final String CREATION_DATE = "creation_date";
    private static final String UPDATE_DATE = "update_date";

    private ProjectLanguageEmbeddedId id;

    private Long lines;

    @CreationTimestamp
    @Column(name = CREATION_DATE, nullable = false)
    private LocalDateTime creationDate;

    @UpdateTimestamp
    @Column(name = UPDATE_DATE, nullable = false)
    private LocalDateTime updateDate;
}