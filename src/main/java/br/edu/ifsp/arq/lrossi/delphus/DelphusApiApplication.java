package br.edu.ifsp.arq.lrossi.delphus;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DelphusApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(DelphusApiApplication.class, args);
	}

}
