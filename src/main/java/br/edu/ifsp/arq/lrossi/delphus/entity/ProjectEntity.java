package br.edu.ifsp.arq.lrossi.delphus.entity;

import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.time.LocalDateTime;

@Data
@Entity(name = ProjectEntity.ENTITY_NAME)
@Table(name = ProjectEntity.TABLE_NAME)
public class ProjectEntity {

    public static final String ENTITY_NAME = "ProjectEntity";
    public static final String TABLE_NAME = "project";
    private static final String ID_PROJECT = "id_project";
    private static final String CREATION_DATE = "creation_date";
    private static final String UPDATE_DATE = "update_date";

    @Id
    @Column(name = ID_PROJECT, nullable = false)
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(length = 50, nullable = false)
    private String name;

    private Long lines;

    @ManyToOne
    @JoinColumn(name = "id_developer")
    private DeveloperEntity owner;

    @CreationTimestamp
    @Column(name = CREATION_DATE, nullable = false)
    private LocalDateTime creationDate;

    @UpdateTimestamp
    @Column(name = UPDATE_DATE, nullable = false)
    private LocalDateTime updateDate;
}