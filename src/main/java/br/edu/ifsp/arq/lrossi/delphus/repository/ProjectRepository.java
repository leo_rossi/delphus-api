package br.edu.ifsp.arq.lrossi.delphus.repository;

import br.edu.ifsp.arq.lrossi.delphus.entity.ProjectEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProjectRepository extends JpaRepository<ProjectEntity, Long> {
}
