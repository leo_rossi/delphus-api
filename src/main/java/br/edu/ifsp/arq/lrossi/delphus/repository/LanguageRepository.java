package br.edu.ifsp.arq.lrossi.delphus.repository;

import br.edu.ifsp.arq.lrossi.delphus.entity.LanguageEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LanguageRepository extends JpaRepository<LanguageEntity, Long> {
}
