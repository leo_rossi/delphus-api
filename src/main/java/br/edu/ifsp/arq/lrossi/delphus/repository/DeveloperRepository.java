package br.edu.ifsp.arq.lrossi.delphus.repository;

import br.edu.ifsp.arq.lrossi.delphus.entity.DeveloperEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DeveloperRepository extends JpaRepository<DeveloperEntity, Long> {

}
