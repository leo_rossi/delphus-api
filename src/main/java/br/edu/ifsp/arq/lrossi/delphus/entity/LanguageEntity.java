package br.edu.ifsp.arq.lrossi.delphus.entity;

import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDateTime;

@Data
@Entity(name = LanguageEntity.ENTITY_NAME)
@Table(name = LanguageEntity.TABLE_NAME)
public class LanguageEntity {

    public static final String ENTITY_NAME = "LanguageEntity";
    public static final String TABLE_NAME = "language";
    private static final String ID_LANGUAGE = "id_language";
    private static final String CREATION_DATE = "creation_date";
    private static final String UPDATE_DATE = "update_date";

    @Id
    @Column(name = ID_LANGUAGE)
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(length = 45, nullable = false)
    private String language;

    @CreationTimestamp
    @Column(name = CREATION_DATE, nullable = false)
    private LocalDateTime creationDate;

    @UpdateTimestamp
    @Column(name = UPDATE_DATE, nullable = false)
    private LocalDateTime updateDate;
}