-- MySQL Script generated by MySQL Workbench
-- Mon 19 Aug 2019 09:23:02 AM -03
-- Model: New Model    Version: 1.0
-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema delphus
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema delphus
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `delphus` ;
USE `delphus` ;

-- -----------------------------------------------------
-- Table `delphus`.`developer`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `delphus`.`developer` (
  `id_developer` INT NOT NULL AUTO_INCREMENT,
  `git_username` VARCHAR(45) NOT NULL,
  `creation_date` DATETIME NULL,
  `update_date` DATETIME NULL,
  PRIMARY KEY (`id_developer`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `delphus`.`language`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `delphus`.`language` (
  `id_language` INT NOT NULL AUTO_INCREMENT,
  `language` VARCHAR(45) NOT NULL,
  `creation_date` DATETIME NULL,
  `update_date` DATETIME NULL,
  PRIMARY KEY (`id_language`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `delphus`.`project`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `delphus`.`project` (
  `id_project` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NULL,
  `id_developer` INT NOT NULL,
  `lines` INT NULL,
  `creation_date` DATETIME NULL,
  `update_date` DATETIME NULL,
  PRIMARY KEY (`id_project`),
  INDEX `fk_project_owner_idx` (`id_developer` ASC) VISIBLE,
  CONSTRAINT `fk_project_owner`
    FOREIGN KEY (`id_developer`)
    REFERENCES `delphus`.`developer` (`id_developer`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `delphus`.`project_language`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `delphus`.`project_language` (
  `id_project` INT NOT NULL,
  `id_language` INT NOT NULL,
  `lines` INT NULL,
  `creation_date` DATETIME NULL,
  `update_date` DATETIME NULL,
  PRIMARY KEY (`id_project`, `id_language`),
  INDEX `fk_projectlanguage_lang_idx` (`id_language` ASC) VISIBLE,
  CONSTRAINT `fk_projectlanguage_proj`
    FOREIGN KEY (`id_project`)
    REFERENCES `delphus`.`project` (`id_project`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_projectlanguage_lang`
    FOREIGN KEY (`id_language`)
    REFERENCES `delphus`.`language` (`id_language`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
